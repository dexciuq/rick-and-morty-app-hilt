package com.dexciuq.rickandmortyapp_hilt.presentation.screen.main

import com.dexciuq.rickandmortyapp_hilt.domain.model.Character

sealed class MainScreenState {
    object Loading : MainScreenState()
    data class Error(val message: String) : MainScreenState()
    data class Success(val characters: List<Character>) : MainScreenState()
}