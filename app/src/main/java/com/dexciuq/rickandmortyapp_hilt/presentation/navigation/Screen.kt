package com.dexciuq.rickandmortyapp_hilt.presentation.navigation

sealed class Screen(val route: String) {
    object Splash : Screen("splash")
    object Main : Screen("main")
}