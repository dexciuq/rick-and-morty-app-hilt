package com.dexciuq.rickandmortyapp_hilt.data.source.remote

import com.dexciuq.rickandmortyapp_hilt.data.mapper.CharacterMapper
import com.dexciuq.rickandmortyapp_hilt.data.source.CharacterDataSource
import com.dexciuq.rickandmortyapp_hilt.domain.model.Character
import com.dexciuq.rickandmortyapp_hilt.domain.model.Filter
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val apiService: RickAndMortyApiService,
    private val mapper: CharacterMapper,
) : CharacterDataSource {

    override suspend fun getCharacter(id: Int): Character = mapper.transform(
        apiService.getSingleCharacter(id)
    )

    override suspend fun getAllCharacters(page: Int): List<Character> = mapper.transformAll(
        apiService.getAllCharacters(page).results
    )

    override suspend fun getAllCharactersByFilter(filter: Filter, page: Int): List<Character> =
        mapper.transformAll(
            apiService.getAllCharactersByFilter(
                name = filter.name,
                status = filter.status,
                species = filter.species,
                type = filter.type,
                gender = filter.gender,
                page = page
            ).results
        )
}