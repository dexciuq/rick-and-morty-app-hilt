package com.dexciuq.rickandmortyapp_hilt.data.repository

import com.dexciuq.rickandmortyapp_hilt.data.source.CharacterDataSource
import com.dexciuq.rickandmortyapp_hilt.domain.model.Character
import com.dexciuq.rickandmortyapp_hilt.domain.model.Filter
import com.dexciuq.rickandmortyapp_hilt.domain.repository.CharacterRepository
import javax.inject.Inject

class CharacterRepositoryImpl @Inject constructor(
    private val characterDataSource: CharacterDataSource
) : CharacterRepository {

    override suspend fun getCharacter(id: Int): Character =
        characterDataSource.getCharacter(id)

    override suspend fun getAllCharacters(page: Int): List<Character> =
        characterDataSource.getAllCharacters(page)

    override suspend fun getAllCharactersByFilter(filter: Filter, page: Int): List<Character> =
        characterDataSource.getAllCharactersByFilter(filter, page)
}