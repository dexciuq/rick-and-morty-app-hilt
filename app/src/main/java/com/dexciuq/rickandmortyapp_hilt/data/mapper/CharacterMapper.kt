package com.dexciuq.rickandmortyapp_hilt.data.mapper

import com.dexciuq.rickandmortyapp_hilt.data.model.CharacterDto
import com.dexciuq.rickandmortyapp_hilt.domain.model.Character
import javax.inject.Inject

class CharacterMapper @Inject constructor() {

    fun transform(param: CharacterDto) = Character(
        id = param.id,
        name = param.name,
        status = param.status,
        species = param.species,
        type = param.type,
        gender = param.gender,
        origin = param.origin.name,
        location = param.location.name,
        image = param.image,
        episode = param.episode,
        url = param.url,
        created = param.created,
    )

    fun transformAll(param: List<CharacterDto>) = param.map(::transform)
}