package com.dexciuq.rickandmortyapp_hilt.di

import com.dexciuq.rickandmortyapp_hilt.data.source.CharacterDataSource
import com.dexciuq.rickandmortyapp_hilt.data.source.remote.RemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataSourceModule {

    @Binds
    fun provideCharacterDataSource(remoteDataSource: RemoteDataSource): CharacterDataSource
}