package com.dexciuq.rickandmortyapp_hilt.di

import com.dexciuq.rickandmortyapp_hilt.data.repository.CharacterRepositoryImpl
import com.dexciuq.rickandmortyapp_hilt.domain.repository.CharacterRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun provideCharacterRepository(characterRepositoryImpl: CharacterRepositoryImpl): CharacterRepository
}