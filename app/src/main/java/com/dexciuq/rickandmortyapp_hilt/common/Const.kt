package com.dexciuq.rickandmortyapp_hilt.common

object Const {
    const val BASE_URL = "https://rickandmortyapi.com"
}