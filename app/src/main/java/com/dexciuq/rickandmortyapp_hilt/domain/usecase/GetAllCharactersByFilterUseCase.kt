package com.dexciuq.rickandmortyapp_hilt.domain.usecase

import com.dexciuq.rickandmortyapp_hilt.domain.model.Filter
import com.dexciuq.rickandmortyapp_hilt.domain.repository.CharacterRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetAllCharactersByFilterUseCase @Inject constructor(
    private val characterRepository: CharacterRepository
) {
    suspend operator fun invoke(filter: Filter, page: Int) = withContext(Dispatchers.IO) {
        characterRepository.getAllCharactersByFilter(filter, page)
    }
}