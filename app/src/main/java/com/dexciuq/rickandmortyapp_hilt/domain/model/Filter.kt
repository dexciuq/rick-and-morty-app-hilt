package com.dexciuq.rickandmortyapp_hilt.domain.model

data class Filter(
    val name: String? = null,
    val status: String? = null,
    val species: String? = null,
    val type: String? = null,
    val gender: String? = null
)