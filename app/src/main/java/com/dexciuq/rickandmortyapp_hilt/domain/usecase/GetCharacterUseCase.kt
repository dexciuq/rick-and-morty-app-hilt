package com.dexciuq.rickandmortyapp_hilt.domain.usecase

import com.dexciuq.rickandmortyapp_hilt.domain.repository.CharacterRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetCharacterUseCase  @Inject constructor(
    private val characterRepository: CharacterRepository
) {
    suspend operator fun invoke(id: Int) = withContext(Dispatchers.IO) {
        characterRepository.getCharacter(id)
    }
}